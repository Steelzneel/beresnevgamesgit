﻿using UnityEngine;
using UnityEngine.Events;

namespace BeresnevGamesTest
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class BallBehaviour : MonoBehaviour
    {
        public Vector2 Direction { private get; set; }
        public ScoreService ScoreService { private get; set; }
        public BallSettings Settings { private get; set; }
        public Color Color { get; set; }

        public UnityAction GameOverEvent;

        private SpriteRenderer _sprite;
        private Rigidbody2D _rigidbody2D;

        private void Awake()
        {
            _sprite = GetComponent<SpriteRenderer>();
            _rigidbody2D = GetComponent<Rigidbody2D>();
        }

        private void Start()
        {
            transform.localScale = Vector2.one * Settings.Size;
            _sprite.color = Color;
        }

        private void Update()
        {
            _rigidbody2D.velocity = Direction * Settings.Speed;
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            switch (collision.collider.tag)
            {
                case "Top Racquet":
                case "Bottom Racquet":
                    Direction = new Vector2(Direction.x, -Direction.y);
                    _rigidbody2D.velocity = Direction * Settings.Speed;
                    ScoreService.Score++;

                    break;

                case "Right Wall":
                case "Left Wall":
                    Direction = new Vector2(-Direction.x, Direction.y);
                    _rigidbody2D.velocity = Direction * Settings.Speed;

                    break;
            }
        }

        private void OnTriggerEnter2D(Collider2D collider)
        {
            if (collider.tag == "GameOver Zone")
            {
                if (GameOverEvent != null)
                {
                    GameOverEvent.Invoke();
                }
            }
        }
    }
}