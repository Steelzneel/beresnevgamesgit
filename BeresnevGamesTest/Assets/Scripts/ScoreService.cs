﻿using System;
using System.IO;
using UnityEngine;
using UnityEngine.Events;

namespace BeresnevGamesTest
{
    public class ScoreService
    {
        public int TopScore { get; set; }
        public int Score
        {
            get => _score;
            set
            {
                _score = value;

                if (ScoreChanged != null)
                {
                    ScoreChanged.Invoke();
                }
            }
        }
        private int _score;
        
        public UnityAction ScoreChanged;

        private readonly string _filepath = Application.persistentDataPath + "/save0.bgtdata";

        public ScoreService()
        {
            Init();
            ScoreChanged += SaveScore;
        }

        private void Init()
        {
            if (File.Exists(_filepath))
            {
                ReadScore();
                return;
            }

            Score = 0;
            TopScore = 0;
        }

        private void ReadScore()
        {
            try
            {
                TopScore = Convert.ToInt32(File.ReadAllText(_filepath));
            }
            catch
            {
                Debug.LogError("Cannot convert string to int");
            }
        }

        private void SaveScore()
        {
            if (Score > TopScore)
            {
                File.WriteAllText(_filepath, Score.ToString());
            }
        }
    }
}