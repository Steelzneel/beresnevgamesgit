﻿using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace BeresnevGamesTest
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _scoreText;
        [SerializeField] private TextMeshProUGUI _topScoreText;
        [SerializeField] private Racquet[] _racquets;
        [SerializeField] private BallBehaviour _ball;
        [SerializeField] private BallSettings[] _settings;
        [SerializeField] private float _racquetSpeed;
        [SerializeField] private GameObject _spriteStuff;
        [SerializeField] private GameObject _mainMenu;
        [SerializeField] private ColorMenu _colorMenu;
        [SerializeField] private GameObject _touchButtonPanel;
        [SerializeField] private TouchButton _leftTouchButton;
        [SerializeField] private TouchButton _rightTouchButton;

        private ScoreService _scoreService;
        private OptionService _optionService;

        private void Awake()
        {
            _scoreService = new ScoreService();
            _scoreService.ScoreChanged += () => _scoreText.text = _scoreService.Score.ToString();
            _topScoreText.text = _scoreService.TopScore.ToString();
            
            foreach (Racquet racquet in _racquets)
            {
                racquet.Speed = _racquetSpeed;
            }

            _optionService = new OptionService();

            _ball.Direction = GetRandomDirection();
            _ball.Settings = _settings[Random.Range(0, _settings.Length)];
            _ball.ScoreService = _scoreService;
            _ball.Color = _optionService.Color;
            _ball.GameOverEvent += () => SceneManager.LoadScene(0);

            _optionService.ColorChanged += () => _ball.Color = _optionService.Color;

            _leftTouchButton.PointerDownHandler += SetMoveLeftBothRacquets;
            _leftTouchButton.PointerUpHandler += StopBothRaquets;

            _rightTouchButton.PointerDownHandler += SetMoveRightBothRacquets;
            _rightTouchButton.PointerUpHandler += StopBothRaquets;
        }

        private void SetMoveLeftBothRacquets()
        {
            foreach (Racquet racquet in _racquets)
            {
                racquet.SetMoveLeft();
            }
        }

        private void SetMoveRightBothRacquets()
        {
            foreach (Racquet racquet in _racquets)
            {
                racquet.SetMoveRight();
            }
        }

        private void StopBothRaquets()
        {
            foreach (Racquet racquet in _racquets)
            {
                racquet.Stop();
            }
        }

        private Vector2 GetRandomDirection()
        {
            Vector2 direction = new Vector2(1f, 1f - Random.Range(0f, 0.3f));

            return direction.normalized;
        }

        public void StartGame()
        {
            _spriteStuff.SetActive(true);
            _mainMenu.SetActive(false);
            _touchButtonPanel.SetActive(true);
        }

        public void OpenColorMenu()
        {
            _mainMenu.SetActive(false);
            _colorMenu.SetActive(_optionService, _mainMenu);
        }
    }
}