﻿using UnityEngine;

namespace BeresnevGamesTest
{
    [CreateAssetMenu(fileName = "New Ball", menuName = "Add New Ball")]
    public class BallSettings : ScriptableObject
    {
        [SerializeField] private float _speed = 1f;
        [SerializeField] private float _size = 1f;

        public float Speed { get => _speed; }
        public float Size { get => _size; }
    }
}