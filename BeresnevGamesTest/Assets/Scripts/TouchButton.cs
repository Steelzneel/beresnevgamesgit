﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace BeresnevGamesTest
{
    public class TouchButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        public UnityAction PointerDownHandler;
        public UnityAction PointerUpHandler;

        public void OnPointerDown(PointerEventData eventData)
        {
            if (PointerDownHandler != null)
            {
                PointerDownHandler.Invoke();
            }
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (PointerUpHandler != null)
            {
                PointerUpHandler.Invoke();
            }
        }
    }
}