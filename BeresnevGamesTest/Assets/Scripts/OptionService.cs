﻿using System;
using System.IO;
using UnityEngine;
using UnityEngine.Events;

namespace BeresnevGamesTest
{
    public class OptionService
    {
        public Color Color
        {
            get => _color;
            set
            {
                _color = value;

                if (ColorChanged != null)
                {
                    ColorChanged.Invoke();
                }
            }
        }
        private Color _color;
        
        public UnityAction ColorChanged;

        private readonly string _filepath = Application.persistentDataPath + "/options.cfg";

        public OptionService()
        {
            Init();
            ColorChanged += SaveOptions;
        }

        private void Init()
        {
            if (File.Exists(_filepath))
            {
                ReadOptions();
                return;
            }

            Color = Color.white;
        }

        private void ReadOptions()
        {
            string data = File.ReadAllText(_filepath);
            Color = JsonUtility.FromJson<Color>(data);
        }

        private void SaveOptions()
        {
            string data = JsonUtility.ToJson(Color);
            File.WriteAllText(_filepath, data);
        }
    }
}