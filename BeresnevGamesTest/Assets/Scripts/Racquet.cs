﻿using UnityEngine;

namespace BeresnevGamesTest
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class Racquet : MonoBehaviour
    {
        public float Speed { get; set; }

        private readonly float _maximumOffset = 5.9f;
        private Rigidbody2D _rigidbody2D;

        private void Awake()
        {
            _rigidbody2D = GetComponent<Rigidbody2D>();
        }

        public void SetMoveLeft()
        {
            _rigidbody2D.velocity = Vector2.left * Speed;
        }

        public void SetMoveRight()
        {
            _rigidbody2D.velocity = Vector2.right * Speed;
        }

        public void Stop()
        {
            _rigidbody2D.velocity = Vector2.zero;
        }

        private void Update()
        {
            if (transform.position.x < -_maximumOffset)
            {
                transform.position = new Vector3(-_maximumOffset, transform.position.y, transform.position.z);
            }
            else if (transform.position.x > _maximumOffset)
            {
                transform.position = new Vector3(_maximumOffset, transform.position.y, transform.position.z);
            }
        }
    }
}