﻿using UnityEngine;
using UnityEngine.UI;

namespace BeresnevGamesTest
{
    public class ColorMenu : MonoBehaviour
    {
        [SerializeField] private Slider _sliderRColor;
        [SerializeField] private Slider _sliderGColor;
        [SerializeField] private Slider _sliderBColor;
        [SerializeField] private Image _colorImage;

        private OptionService _optionService;
        private GameObject _startMenu;

        private void Awake()
        {
            _sliderRColor.onValueChanged.AddListener((data) => _colorImage.color = new Color(data, _colorImage.color.g, _colorImage.color.b));
            _sliderGColor.onValueChanged.AddListener((data) => _colorImage.color = new Color(_colorImage.color.r, data, _colorImage.color.b));
            _sliderBColor.onValueChanged.AddListener((data) => _colorImage.color = new Color(_colorImage.color.r, _colorImage.color.g, data));

            _sliderRColor.value = _optionService.Color.r;
            _sliderGColor.value = _optionService.Color.g;
            _sliderBColor.value = _optionService.Color.b;
        }

        public void SetActive(OptionService optionService, GameObject startMenu)
        {
            _optionService = optionService;
            gameObject.SetActive(true);
            _startMenu = startMenu;
            _startMenu.SetActive(false);
        }

        public void ChangeMenus()
        {
            _startMenu.SetActive(true);
            gameObject.SetActive(false);
        }

        public void SetBallColor()
        {
            _optionService.Color = new Color(_sliderRColor.value, _sliderGColor.value, _sliderBColor.value);
            ChangeMenus();
        }
    }
}